package com.example.login.ui.profile

import android.content.SharedPreferences
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.login.data.LoginRepository
import com.example.login.data.Result

import com.example.login.R

class ProfileViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    private var sharedPreferences:SharedPreferences?=null
    private val _loginForm = MutableLiveData<ProfileFormState>()
    val loginFormState: LiveData<ProfileFormState> = _loginForm

    fun save (text1:String, text2:String) {
        sharedPreferences?.let {
            it.edit().putString("k1",text1).putString("k2",text2).commit()

        }

    }

    fun init (shared:SharedPreferences){
        sharedPreferences = shared
        sharedPreferences?.let {
            val text1 = it.getString("k1","").orEmpty()
            val text2 = it.getString("k2","").orEmpty()

            _loginForm.postValue(ProfileFormState(text1,text2))
        }
    }
}