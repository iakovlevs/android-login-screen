package com.example.login.ui.profile

/**
 * Data validation state of the login form.
 */
class ProfileFormState(

    val text1:String = "",
    val text2:String = ""

)