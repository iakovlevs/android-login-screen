package com.example.login.ui.profile

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import com.example.login.databinding.ActivityLoginBinding

import com.example.login.R
import com.example.login.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {

    private lateinit var loginViewModel: ProfileViewModel
    private lateinit var binding: ActivityProfileBinding
    private lateinit var shared:SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        shared = getPreferences(Context.MODE_PRIVATE)

        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val editText1 = binding.editText
        val editText2 = binding.editText2
        val button = binding.Button

        button.setOnClickListener { loginViewModel.save(editText1.text.toString(),editText2.text.toString()) }

        loginViewModel = ViewModelProvider(this, ProfileViewModelFactory())
            .get(ProfileViewModel::class.java)

        loginViewModel.loginFormState.observe(this@ProfileActivity, Observer {
            val loginState = it ?: return@Observer
            editText1.setText(loginState.text1)
            editText2.setText(loginState.text2)

        })
        loginViewModel.init(shared)
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}