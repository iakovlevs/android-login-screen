package com.example.login.data

import com.example.login.data.model.LoggedInUser
import java.io.IOException

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
class LoginDataSource {

    fun login(username: String, password: String): Result<LoggedInUser> {
        if ( username == "admin@admin.ru" && password == "1234") {
            return Result.Success(LoggedInUser("id","user"))
        } else {
            return Result.Error(IllegalStateException("Please try again"))
        }
    }

    fun logout() {
        // TODO: revoke authentication
    }
}